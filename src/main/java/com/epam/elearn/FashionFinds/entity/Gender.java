package com.epam.elearn.FashionFinds.entity;

import jakarta.persistence.*;
import lombok.AllArgsConstructor;
import lombok.Data;
import lombok.NoArgsConstructor;

import java.util.List;

@Table(name = "gender")
@Data
@Entity
@AllArgsConstructor
@NoArgsConstructor
public class Gender {
    @Id
    @GeneratedValue(strategy = GenerationType.IDENTITY)
    @Column(name = "id")
    private Integer id;
    @Column(name = "name")

    private String name;
    @OneToMany(mappedBy = "gender", targetEntity = Article.class)
    private List<Article> articles;
}
