package com.epam.elearn.FashionFinds.service;

import com.epam.elearn.FashionFinds.entity.Article;

import java.util.List;

public interface ProductService {
    List<Article> getAllArticles();

    Article getArticleById(Long id);

    List<Article> getArticlesByProductType(String productType);

    List<Article> filterArticlesByGender(String filter);

    void editArticle(Article article);

}
