package com.epam.elearn.FashionFinds.service;

import com.epam.elearn.FashionFinds.dto.CustomerDTO;
import com.epam.elearn.FashionFinds.entity.Customer;

import java.util.List;

public interface CustomerService {
    Customer getCurrentCustomer();

    List<CustomerDTO> getAllCustomers();

}
