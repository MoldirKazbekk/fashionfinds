package com.epam.elearn.FashionFinds.dto;

import jakarta.validation.constraints.Email;
import jakarta.validation.constraints.NotBlank;
import jakarta.validation.constraints.NotEmpty;
import jakarta.validation.constraints.NotNull;
import lombok.Data;

@Data
public class CustomerForm {
    @NotNull
    private String firstName;
    @NotNull
    private String lastName;
    @NotNull
    @NotEmpty(message = "Please provide the username")
    private String username;
    @Email(message = "Please enter correct email address.")
    @NotNull
    private String email;
    private Character gender;
    @NotNull
    private String password1;
    @NotNull
    private String password2;
}
