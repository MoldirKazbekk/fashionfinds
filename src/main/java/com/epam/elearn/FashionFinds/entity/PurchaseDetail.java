package com.epam.elearn.FashionFinds.entity;

import jakarta.persistence.*;
import lombok.AllArgsConstructor;
import lombok.Data;
import lombok.NoArgsConstructor;

@Table(name = "purchase_detail")
@Entity
@Data
@AllArgsConstructor
@NoArgsConstructor
public class PurchaseDetail {
    @Id
    @GeneratedValue(strategy = GenerationType.IDENTITY)
    @Column(name = "purchase_detail_id")
    private Integer purchaseDetailId;

    @ManyToOne
    @JoinColumn(name = "purchase_id")
    private Purchase purchase;

    @Column(name = "article_code")
    private Long articleCode;

    @Column(name = "size_code")
    private String sizeCode;

    @Column(name = "amount")
    private Integer amount;

    @Column(name = "price")
    private Double price;
}
