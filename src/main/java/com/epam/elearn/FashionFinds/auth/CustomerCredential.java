package com.epam.elearn.FashionFinds.auth;

import com.epam.elearn.FashionFinds.entity.Customer;
import lombok.Getter;
import lombok.RequiredArgsConstructor;
import org.springframework.security.core.GrantedAuthority;
import org.springframework.security.core.authority.SimpleGrantedAuthority;
import org.springframework.security.core.userdetails.UserDetails;

import java.util.Collection;
import java.util.Collections;
import java.util.HashSet;

@RequiredArgsConstructor
@Getter
public class CustomerCredential implements UserDetails {
    private final Customer customer;

    @Override
    public Collection<? extends GrantedAuthority> getAuthorities() {
        if(customer.getCustomerRoles()==null || customer.getCustomerRoles().isEmpty()){
            return Collections.emptySet();
        }
        Collection<SimpleGrantedAuthority> grantedAuthorities = new HashSet<>();
        customer.getCustomerRoles().forEach(customerRole -> {
            grantedAuthorities.add(new SimpleGrantedAuthority(customerRole.getRole()));
        });
        return grantedAuthorities;
    }


    @Override
    public String getPassword() {
        return this.customer.getPassword();
    }

    @Override
    public String getUsername() {
        return this.customer.getUsername();
    }

    @Override
    public boolean isAccountNonExpired() {
        return true;
    }

    @Override
    public boolean isAccountNonLocked() {
        return true;
    }

    @Override
    public boolean isCredentialsNonExpired() {
        return true;
    }

    @Override
    public boolean isEnabled() {
        return true;
    }
}
