package com.epam.elearn.FashionFinds.dto;

import com.epam.elearn.FashionFinds.entity.Article;
import lombok.Builder;
import lombok.Data;

@Data
@Builder
public class ProductInCart {
    private Article article;

    private String sizeCode;

    private Integer amount;

    private Double price;
}
