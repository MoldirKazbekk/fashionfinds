package com.epam.elearn.FashionFinds.service.impl;

import com.epam.elearn.FashionFinds.dto.ProductInCart;
import com.epam.elearn.FashionFinds.entity.*;
import com.epam.elearn.FashionFinds.repository.ArticleRepository;
import com.epam.elearn.FashionFinds.repository.ArticleSizeRepository;
import com.epam.elearn.FashionFinds.repository.PurchaseDetailRepository;
import com.epam.elearn.FashionFinds.repository.PurchaseRepository;
import com.epam.elearn.FashionFinds.service.CustomerService;
import com.epam.elearn.FashionFinds.service.ProductService;
import com.epam.elearn.FashionFinds.service.PurchaseService;
import lombok.RequiredArgsConstructor;
import org.springframework.stereotype.Service;

import java.time.LocalDate;
import java.util.List;

@Service
@RequiredArgsConstructor
public class PurchaseServiceImpl implements PurchaseService {
    private final ProductService productService;
    private final CustomerService customerService;
    private final PurchaseDetailRepository purchaseDetailRepository;
    private final PurchaseRepository purchaseRepository;
    private final ArticleRepository articleRepository;
    private final ArticleSizeRepository articleSizeRepository;
    @Override
    public boolean addProductToCart(Long articleId, String size, Integer amount) {

        Article chosenArticle = articleRepository.findById(articleId).get();
        int subtract = chosenArticle.getArticleSizeList().stream().filter(articleSize ->
             articleSize.getArticleSizeCompositeKey().getSizeCode().equalsIgnoreCase(size)
        ).findAny().get().getAmount() - amount;
        if(subtract<0){
            return false;
        }
        for(ProductInCart product: customerService.getCurrentCustomer().getProductInCartList()){
            if(product.getSizeCode().equalsIgnoreCase(size) && product.getArticle().getId().equals(articleId)){
                product.setAmount(product.getAmount() + amount);
                return true;
            }
        }
        customerService.getCurrentCustomer().getProductInCartList().add(
                ProductInCart.builder()
                .article(chosenArticle)
                .amount(amount)
                .sizeCode(size)
                .price(productService.getArticleById(articleId).getPrice())
                .build()
        );
        return true;
    }

    @Override
    public List<ProductInCart> getProductsInCart() {
        return customerService.getCurrentCustomer().getProductInCartList();
    }

    @Override
    public void createPurchase() {
        Customer customer = customerService.getCurrentCustomer();
        List<ProductInCart> itemsInCart = getProductsInCart();
        Purchase purchase = new Purchase();
        purchase.setCustomer(customer);
        purchase.setTotalSum(getTotalSum());
        purchase.setPurchasedDate(LocalDate.now());
        Purchase addedPurchase = purchaseRepository.save(purchase);
        for (ProductInCart product : itemsInCart) {
            PurchaseDetail purchaseDetail = new PurchaseDetail();
            Article article = product.getArticle();
            for (ArticleSize articleSize : article.getArticleSizeList()) {
                if(articleSize.getArticleSizeCompositeKey().getSizeCode().equalsIgnoreCase(product.getSizeCode())){
                    articleSize.setAmount(articleSize.getAmount()-product.getAmount());
                    articleSizeRepository.save(articleSize);
                    break;
                }
            }
            purchaseDetail.setPurchase(addedPurchase);
            purchaseDetail.setAmount(product.getAmount());
            purchaseDetail.setSizeCode(product.getSizeCode());
            purchaseDetail.setArticleCode(product.getArticle().getId());
            purchaseDetail.setPrice(product.getPrice());
            purchaseDetailRepository.save(purchaseDetail);
        }
        customer.getProductInCartList().clear();
    }

    @Override
    public Double getTotalSum() {
        Double total = 0.0;
        for (ProductInCart product : getProductsInCart()) {
            total += product.getPrice() * product.getAmount();
        }
        return total;
    }


    @Override
    public List<ProductInCart> removeItemInCart(Long articleId, String sizeCode) {
        List<ProductInCart> products = customerService.getCurrentCustomer().getProductInCartList();
        for(ProductInCart product:products){
            if(product.getArticle().getId().equals(articleId) && product.getSizeCode().equalsIgnoreCase(sizeCode)){
                products.remove(product);
                break;
            }
        }
        return products;
    }
}
