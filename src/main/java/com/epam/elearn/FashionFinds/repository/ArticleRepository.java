package com.epam.elearn.FashionFinds.repository;

import com.epam.elearn.FashionFinds.entity.Article;
import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.stereotype.Repository;

@Repository
public interface ArticleRepository extends JpaRepository<Article, Long> {

}
