package com.epam.elearn.FashionFinds.auth;

import com.epam.elearn.FashionFinds.entity.Customer;
import com.epam.elearn.FashionFinds.repository.CustomerRepository;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.security.core.userdetails.UserDetails;
import org.springframework.security.core.userdetails.UserDetailsService;
import org.springframework.security.core.userdetails.UsernameNotFoundException;
import org.springframework.stereotype.Service;

@Service
public class CustomerDetailsService implements UserDetailsService {
    @Autowired
    private CustomerRepository customerRepository;

    @Override
    public UserDetails loadUserByUsername(String username) throws UsernameNotFoundException {
        Customer customer = customerRepository.findByUsername(username);
        if(customer==null){
            throw new UsernameNotFoundException("User with username - " + username + " not found");
        }
        return new CustomerCredential(customer);
    }
}
