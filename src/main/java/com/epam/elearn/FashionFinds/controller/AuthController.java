package com.epam.elearn.FashionFinds.controller;

import com.epam.elearn.FashionFinds.dto.CustomerForm;
import com.epam.elearn.FashionFinds.dto.LoginForm;
import com.epam.elearn.FashionFinds.service.impl.AuthServiceImpl;
import jakarta.validation.Valid;
import lombok.RequiredArgsConstructor;
import lombok.extern.slf4j.Slf4j;
import org.springframework.security.authentication.AuthenticationManager;
import org.springframework.stereotype.Controller;
import org.springframework.ui.Model;
import org.springframework.validation.BindingResult;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.ModelAttribute;
import org.springframework.web.bind.annotation.PostMapping;

@Controller
@Slf4j
@RequiredArgsConstructor
public class AuthController {
    private final AuthenticationManager authenticationManager;
    private final AuthServiceImpl authService;

    @PostMapping("/signUp")
    public String handleSignUpSubmission(@Valid @ModelAttribute(value = "customerForm") CustomerForm customerForm,
                                         BindingResult bindingResult,
                                         Model model) {
        if (authService.checkUsernameAndPasswordRetype(customerForm, model) || bindingResult.hasErrors()) {
            model.addAttribute("customerForm", customerForm);
            return "registration";
        }
        authService.registerCustomer(customerForm);
        log.info("user  - {} is logged in", customerForm.getUsername());
        return "redirect:/login";
    }

    @GetMapping("/signUp")
    public String showSignUp(Model model) {
        CustomerForm customerForm = new CustomerForm();
        model.addAttribute("customerForm", customerForm);
        return "registration";
    }

    @GetMapping("/login")
    public String loginPage(Model model) {
        LoginForm loginForm = new LoginForm();
        model.addAttribute("loginForm", loginForm);
        return "login";
    }
}
