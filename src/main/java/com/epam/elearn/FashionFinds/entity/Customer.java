package com.epam.elearn.FashionFinds.entity;

import com.epam.elearn.FashionFinds.dto.ProductInCart;
import jakarta.persistence.*;
import lombok.Data;
import lombok.NoArgsConstructor;

import java.util.ArrayList;
import java.util.List;

@Table(name = "customer")
@Data
@Entity
@NoArgsConstructor
public class Customer { //customer to customer credentials
    @Id
    @Column(name = "id")
    @GeneratedValue(strategy = GenerationType.IDENTITY)
    private Integer id;
    @Column(name = "first_name")
    private String firstName;
    @Column(name = "last_name")
    private String lastName;
    @Column(name = "username", nullable = false, unique = true)
    private String username;
    @Column(name = "email")
    private String email;
    @Column(name = "password", nullable = false)
    private String password;
    @Column(name = "gender")
    private Character gender;
    @OneToMany(fetch = FetchType.EAGER, mappedBy = "customer")
    private List<Purchase> purchases;
    @OneToMany(fetch = FetchType.EAGER, mappedBy = "customer")
    private List<CustomerRole> customerRoles;
    @Transient
    private List<ProductInCart> productInCartList = new ArrayList<>();
}
