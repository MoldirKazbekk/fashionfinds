package com.epam.elearn.FashionFinds.repository;

import com.epam.elearn.FashionFinds.entity.Gender;
import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.stereotype.Repository;

@Repository
public interface GenderRepository extends JpaRepository<Gender, Integer> {
    Gender findByName(String name);
}
