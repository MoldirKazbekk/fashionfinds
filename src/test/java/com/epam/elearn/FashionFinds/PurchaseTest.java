package com.epam.elearn.FashionFinds;

import com.epam.elearn.FashionFinds.dto.ProductInCart;
import com.epam.elearn.FashionFinds.entity.*;
import com.epam.elearn.FashionFinds.repository.ArticleRepository;
import com.epam.elearn.FashionFinds.repository.ArticleSizeRepository;
import com.epam.elearn.FashionFinds.repository.PurchaseDetailRepository;
import com.epam.elearn.FashionFinds.repository.PurchaseRepository;
import com.epam.elearn.FashionFinds.service.CustomerService;
import com.epam.elearn.FashionFinds.service.PurchaseService;
import org.junit.jupiter.api.Assertions;
import org.junit.jupiter.api.Test;
import org.mockito.Mockito;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.boot.test.context.SpringBootTest;
import org.springframework.boot.test.mock.mockito.MockBean;
import org.springframework.security.core.parameters.P;

import java.util.ArrayList;
import java.util.List;
import java.util.Optional;

import static org.mockito.Mockito.verify;
import static org.mockito.Mockito.when;

@SpringBootTest
public class PurchaseTest {
    @MockBean
    private ArticleRepository articleRepository;
    @MockBean
    private CustomerService customerService;
    @Autowired
    private PurchaseService purchaseService;

    @Test
    void checkAddProductToCartHappyCase() {
        Article article = new Article();
        article.setId(12345L);
        article.setPrice(12000.0);
        ArticleSize articleSize = new ArticleSize();
        articleSize.setArticleSizeCompositeKey(new ArticleSizeCompositeKey(12345L, "S"));
        articleSize.setAmount(10);
        article.setArticleSizeList(new ArrayList<>());
        article.getArticleSizeList().add(articleSize);
        when(articleRepository.findById(Mockito.anyLong()))
                .thenReturn(Optional.of(article));
        Customer customer = new Customer();
        when(customerService.getCurrentCustomer()).thenReturn(customer);

        Assertions.assertTrue(purchaseService.addProductToCart(12345L, "S", 5));
        Assertions.assertEquals(1, customer.getProductInCartList().size());
        Assertions.assertTrue(purchaseService.addProductToCart(12345L, "S", 5));
        Assertions.assertEquals(1, customer.getProductInCartList().size());
        double expectedTotalSum = 0.0;
        for (ProductInCart product : customer.getProductInCartList()) {
            expectedTotalSum += product.getPrice() * product.getAmount();
        }
        Assertions.assertEquals(expectedTotalSum, purchaseService.getTotalSum());

        Assertions.assertFalse(purchaseService.addProductToCart(12345L, "S", articleSize.getAmount() + 1));
    }

    @Test
    void checkRemoveItemInCart() {
        Customer customer = new Customer();
        customer.setProductInCartList(new ArrayList<>());
        Article article = new Article();
        article.setId(12345L);
        article.setPrice(15000.0);
        article.setArticleSizeList(new ArrayList<>());
        ArticleSize articleSize = new ArticleSize();
        articleSize.setArticleSizeCompositeKey(new ArticleSizeCompositeKey(12345L, "S"));
        articleSize.setAmount(11);
        article.getArticleSizeList().add(articleSize);
        when(articleRepository.findById(Mockito.anyLong()))
                .thenReturn(Optional.of(article));
        when(customerService.getCurrentCustomer()).thenReturn(customer);
        purchaseService.addProductToCart(article.getId(), articleSize.getArticleSizeCompositeKey().getSizeCode(),
                5);
        List<ProductInCart> notRemovedList =
                purchaseService.removeItemInCart(54321L, articleSize.getArticleSizeCompositeKey().getSizeCode());
        Assertions.assertEquals(1, notRemovedList.size());
        List<ProductInCart> actualList =
                purchaseService.removeItemInCart(article.getId(), articleSize.getArticleSizeCompositeKey().getSizeCode());
        Assertions.assertEquals(0, actualList.size());
    }

    @Test
    void checkCreatePurchase() {
        Customer customer = new Customer();
        customer.setId(1);

        Article article = new Article();
        article.setId(15123L);
        article.setPrice(15000.0);
        article.setArticleSizeList(new ArrayList<>());
        ArticleSize articleSize = new ArticleSize();
        articleSize.setArticleSizeCompositeKey(new ArticleSizeCompositeKey(article.getId(), "S"));
        articleSize.setAmount(11);
        article.getArticleSizeList().add(articleSize);
        List<ProductInCart> productInCartList = new ArrayList<>();
        ProductInCart product = ProductInCart.builder()
                .article(article)
                .sizeCode("S")
                .amount(5)
                .price(article.getPrice())
                .build();
        int expectedAmount = article.getArticleSizeList().get(0).getAmount() - product.getAmount();
        productInCartList.add(product);
        customer.setProductInCartList(productInCartList);
        when(customerService.getCurrentCustomer()).thenReturn(customer);
        double totalSum = purchaseService.getTotalSum();
        purchaseService.createPurchase();
//        Assertions.assertEquals(expectedAmount, article.getArticleSizeList().get(0).getAmount());
//        List<Purchase> purchases = purchaseRepository.findPurchaseByTotalSumAndCustomerId(totalSum, customer.getId());
//        Purchase lastPurchase = purchases.get(purchases.size() - 1);
//
//        Assertions.assertNotNull(lastPurchase.getPurchaseDetails());

        Assertions.assertTrue(customer.getProductInCartList().isEmpty());

    }
}
