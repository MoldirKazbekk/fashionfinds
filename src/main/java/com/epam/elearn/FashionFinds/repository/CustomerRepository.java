package com.epam.elearn.FashionFinds.repository;

import com.epam.elearn.FashionFinds.entity.Customer;
import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.stereotype.Repository;

@Repository
public interface CustomerRepository extends JpaRepository<Customer, Integer> {
    Customer findByUsername(String username);
    Boolean existsCustomerByUsername(String username);
}
