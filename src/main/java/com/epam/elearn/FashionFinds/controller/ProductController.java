package com.epam.elearn.FashionFinds.controller;

import com.epam.elearn.FashionFinds.service.ProductService;
import lombok.RequiredArgsConstructor;
import org.springframework.stereotype.Controller;
import org.springframework.ui.Model;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.PathVariable;

@Controller
@RequiredArgsConstructor
public class ProductController {
    private final ProductService productService;
    @GetMapping("/article/{id}")
    public String viewProduct(@PathVariable("id") Long articleId, Model model){
        model.addAttribute("article", productService.getArticleById(articleId));
        return "article";
    }
}
