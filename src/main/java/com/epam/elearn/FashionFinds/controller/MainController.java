package com.epam.elearn.FashionFinds.controller;

import com.epam.elearn.FashionFinds.service.ProductService;
import lombok.RequiredArgsConstructor;
import org.springframework.stereotype.Controller;
import org.springframework.ui.Model;
import org.springframework.web.bind.annotation.GetMapping;

@Controller
@RequiredArgsConstructor
public class MainController {

    private final ProductService productService;
    private static final String ARTICLES_LIST = "articlesList";
    private static final String INDEX = "index";


    @GetMapping("/")
    public String indexPage(Model model) {
        model.addAttribute(ARTICLES_LIST, productService.getAllArticles());
        return INDEX;
    }

    @GetMapping("/filter/all")
    public String filterAll() {
        return "redirect:/";
    }

    // Mapping for "Men" filter
    @GetMapping("/filter/men")
    public String filterMen(Model model) {
        model.addAttribute(ARTICLES_LIST, productService.filterArticlesByGender("male"));
        return INDEX;
    }

    // Mapping for "Women" filter
    @GetMapping("/filter/women")
    public String filterWomen(Model model) {
        model.addAttribute(ARTICLES_LIST, productService.filterArticlesByGender("female"));
        return INDEX;
    }

    // Mapping for "unisex" filter
    @GetMapping("/filter/unisex")
    public String filterUnisex(Model model) {
        model.addAttribute(ARTICLES_LIST, productService.filterArticlesByGender("unisex"));
        return INDEX;
    }
}
