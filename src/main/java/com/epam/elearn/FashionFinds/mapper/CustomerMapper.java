package com.epam.elearn.FashionFinds.mapper;

import com.epam.elearn.FashionFinds.dto.CustomerForm;
import com.epam.elearn.FashionFinds.entity.Customer;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.security.crypto.bcrypt.BCryptPasswordEncoder;
import org.springframework.stereotype.Service;

@Service
public class CustomerMapper {
    @Autowired
    private BCryptPasswordEncoder passwordEncoder;

    public  Customer customerFormToCustomer(CustomerForm customerForm){
        Customer customer = new Customer();
        customer.setEmail(customerForm.getEmail());
        customer.setFirstName(customerForm.getFirstName());
        customer.setLastName(customerForm.getLastName());
        customer.setGender(customerForm.getGender());
        customer.setUsername(customerForm.getUsername());
        customer.setPassword(passwordEncoder.encode(customerForm.getPassword1()));
        return customer;
    }
}
