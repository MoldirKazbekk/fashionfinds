package com.epam.elearn.FashionFinds.repository;

import com.epam.elearn.FashionFinds.entity.Purchase;
import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.stereotype.Repository;

import java.util.List;

@Repository
public interface PurchaseRepository extends JpaRepository<Purchase, Integer> {
    List<Purchase> findPurchaseByTotalSumAndCustomerId(Double totalSum, Integer customerId);
}
