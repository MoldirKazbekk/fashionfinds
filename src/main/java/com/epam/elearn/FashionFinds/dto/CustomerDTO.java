package com.epam.elearn.FashionFinds.dto;

import lombok.Builder;
import lombok.Data;

@Data
@Builder
public class CustomerDTO {
    private String username;
    private String firstName;
    private String lastName;
    private String gender;
    private Double totalSum;
}
