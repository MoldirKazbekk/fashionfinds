package com.epam.elearn.FashionFinds.entity;

import jakarta.persistence.*;
import lombok.AllArgsConstructor;
import lombok.Data;
import lombok.NoArgsConstructor;

import java.util.List;

@Table(name="product_type")
@Data
@Entity
@AllArgsConstructor
@NoArgsConstructor
public class ProductType {
    @Id
    @Column(name = "id")
    @GeneratedValue(strategy = GenerationType.IDENTITY)
    private Integer id;
    @Column(name = "name")
    private String name;
    @OneToMany(mappedBy = "productType", targetEntity = Article.class)
    private List<Article> articles;
}
