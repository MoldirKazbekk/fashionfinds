package com.epam.elearn.FashionFinds.service.impl;

import com.epam.elearn.FashionFinds.auth.CustomerCredential;
import com.epam.elearn.FashionFinds.dto.CustomerDTO;
import com.epam.elearn.FashionFinds.entity.Customer;
import com.epam.elearn.FashionFinds.entity.Purchase;
import com.epam.elearn.FashionFinds.repository.CustomerRepository;
import com.epam.elearn.FashionFinds.service.CustomerService;
import lombok.RequiredArgsConstructor;
import org.springframework.security.core.Authentication;
import org.springframework.security.core.context.SecurityContextHolder;
import org.springframework.stereotype.Service;

import java.util.ArrayList;
import java.util.List;
@RequiredArgsConstructor
@Service
public class CustomerServiceImpl implements CustomerService {
    private final CustomerRepository customerRepository;
    @Override
    public Customer getCurrentCustomer() {
        Authentication authentication = SecurityContextHolder.getContext().getAuthentication();
        CustomerCredential credentials = (CustomerCredential) authentication.getPrincipal();
        Customer customer = credentials.getCustomer();
        return customer;
    }

    @Override
    public List<CustomerDTO> getAllCustomers() {
        List<CustomerDTO> list = new ArrayList<>();
        for(Customer customer: customerRepository.findAll()){
            CustomerDTO customerDTO = CustomerDTO.builder()
                    .firstName(customer.getFirstName())
                    .lastName(customer.getLastName())
                    .username(customer.getUsername())
                    .gender(customer.getGender().toString())
                    .totalSum(customer.getPurchases().stream().mapToDouble(Purchase::getTotalSum).sum())
                    .build();
            list.add(customerDTO);
        }
        return list;
    }
}
