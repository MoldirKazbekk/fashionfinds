insert into product_type (name) values ('blouse');
insert into product_type (name) values ('pants');
insert into product_type (name) values ('sweater');
insert into product_type (name) values ('jacket');
insert into product_type (name) values ('shorts');
insert into product_type (name) values ('skirt');
insert into product_type (name) values ('hoodie');
insert into product_type (name) values ('jeans');
insert into product_type (name) values ('cardigan');
insert into product_type (name) values ('blazer');

insert into customer(id,
                     first_name ,
                     last_name,
                     gender ,
                     username ,
                     password,
                     email )
                values (1, 'Moldir', 'Kazbek', 'F', 'moldirka', '$2a$12$o5HTlvNosr5iXlXenM8xjuXflx8HhUM4tV2bRIoighj31xG75hQ3W',
                'moldirkazbek34@gmail.com');
insert into customer_role(customer_id, role) values(1, 'ADMIN');


insert into season (name) values ('spring');
insert into season (name) values ('summer');
insert into season (name) values ('fall');
insert into season (name) values ('winter');

insert into gender (name) values ('female');
insert into gender (name) values ('male');
insert into gender (name) values ('unisex');

insert into article (article_code, type_id, season_id, price, country, gender_id, description) values(15123, 1,
1, 15000, 'Portugal', 1,
'This blouse features a vibrant floral print, perfect for adding a pop of color to your outfit. It has a relaxed fit with ruffled sleeves, giving it a feminine and playful touch.'
);
insert into article (article_code, type_id, season_id, price, country, gender_id, description) values(65134, 2,
4, 52320, 'Turkey', 3,
'These pants are designed for both comfort and style. Made from a stretchy and breathable fabric, they feature a high-rise waist and a wide-leg silhouette, creating a flattering and elongating effect.'
);
insert into article (article_code, type_id, season_id, price, country, gender_id, description) values(17127, 3,
4, 45125, 'Kazakhstan', 1,
'This sweater is the epitome of cozy chic. Crafted from a soft and plush cashmere blend, it features a relaxed turtleneck and oversized sleeves for a relaxed and comfortable fit.'
);
insert into article (article_code, type_id, season_id, price, country, gender_id, description) values(85517, 4,
3, 31000, 'USA', 2,
'This jacket is a perfect blend of fashion and functionality. It is made from a durable waterproof material, making it ideal for outdoor adventures. The jacket features a removable hood and adjustable waist drawstrings, allowing for a customizable fit.'
);
insert into article (article_code, type_id, season_id, price, country, gender_id, description) values(25185, 5,
2, 20000, 'China', 1,
'These shorts are designed with a vibrant and tropical print, perfect for embracing the summer season. Made from a lightweight and breathable fabric, they feature a high-waisted silhouette and a relaxed fit, providing both comfort and style.'
);
insert into article (article_code, type_id, season_id, price, country, gender_id, description) values(25187, 6,
1, 18590, 'India', 1,
'This skirt is a versatile and elegant addition to any wardrobe. It is crafted from a luxurious silk fabric, draping gracefully and flowing with every step. The skirt has a high waistline and a flared silhouette, creating a flattering shape.'
);
insert into article (article_code, type_id, season_id, price, country, gender_id, description) values(25491, 7,
2, 54215, 'Italy', 3,
'This hoodie is the epitome of athleisure style. Made from a soft and cozy cotton blend, it features a relaxed fit and a drawstring hood, providing both comfort and functionality.'
);
insert into article (article_code, type_id, season_id, price, country, gender_id, description) values(54127, 8,
4, 31120, 'Korea', 2,
'These jeans are a wardrobe staple that combines comfort and style effortlessly. Made from high-quality denim, they feature a classic straight-leg fit and a mid-rise waist.'
);
insert into article (article_code, type_id, season_id, price, country, gender_id, description) values(85114, 9,
3, 22450, 'Spain', 1,
'This cardigan is a cozy and versatile layering piece. It is crafted from a soft and lightweight knit fabric, perfect for transitioning between seasons. The cardigan has an open-front design with a draped silhouette, making it effortlessly chic.'
);
insert into article (article_code, type_id, season_id, price, country, gender_id, description) values(21549, 10,
3, 74120, 'Brazil', 2,
'This blazer is a must-have for any polished and professional look. Made from a high-quality wool blend, it features a tailored fit and structured shoulders, creating a sharp and refined silhouette.'
);
insert into article (article_code, type_id, season_id, price, country, gender_id, description) values(15128, 1,
2, 25000, 'Ukraine', 1,
'This blouse perfectly features a vibrant floral print, perfect for adding a pop of color to your outfit. It has a relaxed fit with ruffled sleeves, giving it a feminine and playful touch.'
);

insert into article_size (article_id, size_code, amount) values (15123, 'xs', 7);
insert into article_size (article_id, size_code, amount) values (15123, 's', 11);
insert into article_size (article_id, size_code, amount) values (15123, 'm', 9);
insert into article_size (article_id, size_code, amount) values (15123, 'l', 21);
insert into article_size (article_id, size_code, amount) values (65134, 'xs', 30);
insert into article_size (article_id, size_code, amount) values (65134, 's', 1);
insert into article_size (article_id, size_code, amount) values (65134, 'm', 25);
insert into article_size (article_id, size_code, amount) values (65134, 'l', 4);
insert into article_size (article_id, size_code, amount) values (17127, 'xs', 12);
insert into article_size (article_id, size_code, amount) values (17127, 's', 11);
insert into article_size (article_id, size_code, amount) values (17127, 'm', 18);
insert into article_size (article_id, size_code, amount) values (17127, 'l', 27);
insert into article_size (article_id, size_code, amount) values (85517, 'xs', 3);
insert into article_size (article_id, size_code, amount) values (85517, 's', 20);
insert into article_size (article_id, size_code, amount) values (85517, 'm', 21);
insert into article_size (article_id, size_code, amount) values (85517, 'l', 13);
insert into article_size (article_id, size_code, amount) values (25185, 'xs', 3);
insert into article_size (article_id, size_code, amount) values (25185, 's', 14);
insert into article_size (article_id, size_code, amount) values (25185, 'm', 16);
insert into article_size (article_id, size_code, amount) values (25185, 'l', 24);
insert into article_size (article_id, size_code, amount) values (25187, 'xs', 10);
insert into article_size (article_id, size_code, amount) values (25187, 's', 12);
insert into article_size (article_id, size_code, amount) values (25187, 'm', 25);
insert into article_size (article_id, size_code, amount) values (25187, 'l', 29);
insert into article_size (article_id, size_code, amount) values (25491, 'xs', 18);
insert into article_size (article_id, size_code, amount) values (25491, 's', 11);
insert into article_size (article_id, size_code, amount) values (25491, 'm', 5);
insert into article_size (article_id, size_code, amount) values (25491, 'l', 28);
insert into article_size (article_id, size_code, amount) values (54127, 'xs', 4);
insert into article_size (article_id, size_code, amount) values (54127, 's', 10);
insert into article_size (article_id, size_code, amount) values (54127, 'm', 30);
insert into article_size (article_id, size_code, amount) values (54127, 'l', 8);
insert into article_size (article_id, size_code, amount) values (85114, 'xs', 4);
insert into article_size (article_id, size_code, amount) values (85114, 's', 27);
insert into article_size (article_id, size_code, amount) values (85114, 'm', 6);
insert into article_size (article_id, size_code, amount) values (85114, 'l', 4);
insert into article_size (article_id, size_code, amount) values (21549, 'xs', 17);
insert into article_size (article_id, size_code, amount) values (21549, 's', 3);
insert into article_size (article_id, size_code, amount) values (21549, 'm', 9);
insert into article_size (article_id, size_code, amount) values (21549, 'l', 29);

commit;



