package com.epam.elearn.FashionFinds.entity;

import jakarta.persistence.*;
import lombok.AllArgsConstructor;
import lombok.Data;
import lombok.NoArgsConstructor;

@Table(name = "article_size")
@Entity
@Data
@AllArgsConstructor
@NoArgsConstructor
public class ArticleSize {
    @EmbeddedId
    private ArticleSizeCompositeKey articleSizeCompositeKey;

    @Column(name = "amount")
    private Integer amount;

    @ManyToOne
    @JoinColumn(name = "article_id", insertable = false, updatable = false)
    private Article article;

}
