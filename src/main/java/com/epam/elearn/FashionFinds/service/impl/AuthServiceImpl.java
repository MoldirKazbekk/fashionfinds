package com.epam.elearn.FashionFinds.service.impl;

import com.epam.elearn.FashionFinds.dto.CustomerForm;
import com.epam.elearn.FashionFinds.entity.Customer;
import com.epam.elearn.FashionFinds.mapper.CustomerMapper;
import com.epam.elearn.FashionFinds.repository.CustomerRepository;
import com.epam.elearn.FashionFinds.service.AuthService;
import groovy.util.logging.Slf4j;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;
import org.springframework.ui.Model;

@Service
@Slf4j
public class AuthServiceImpl implements AuthService {
    @Autowired
    private CustomerMapper customerMapper;

    private final Logger logger = LoggerFactory.getLogger(AuthServiceImpl.class);
    private static final String PASSWORD_ERROR_MESSAGE = "Passwords are not identical. Please, try again!";
    private static final String USERNAME_ERROR_MESSAGE = "Username already exists";
    @Autowired
    private CustomerRepository customerRepository;

    public void registerCustomer(CustomerForm customerForm) {
        Customer customer = customerMapper.customerFormToCustomer(customerForm);
        Customer createdCustomer = customerRepository.save(customer);
        logger.info("Created new user: {}", createdCustomer);
    }

    @Override
    public boolean validatePassword(CustomerForm customerForm) {
        return !customerForm.getPassword1().equals(customerForm.getPassword2());
    }

    @Override
    public boolean isUserAlreadyExist(CustomerForm customerForm) {
        return customerRepository.existsCustomerByUsername(customerForm.getUsername());
    }

    @Override
    public boolean checkUsernameAndPasswordRetype(CustomerForm customerForm, Model model) {
        boolean result = false;
        if (validatePassword(customerForm)) {
            model.addAttribute("passwordError", PASSWORD_ERROR_MESSAGE);
            result = true;
        }
        if (isUserAlreadyExist(customerForm)) {
            model.addAttribute("usernameError", USERNAME_ERROR_MESSAGE);
            result = true;
        }
        return result;
    }

}
