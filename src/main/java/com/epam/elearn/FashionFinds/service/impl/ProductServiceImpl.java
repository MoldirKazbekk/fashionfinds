package com.epam.elearn.FashionFinds.service.impl;

import com.epam.elearn.FashionFinds.entity.Article;
import com.epam.elearn.FashionFinds.entity.ProductType;
import com.epam.elearn.FashionFinds.repository.ArticleRepository;
import com.epam.elearn.FashionFinds.repository.GenderRepository;
import com.epam.elearn.FashionFinds.repository.ProductTypeRepository;
import com.epam.elearn.FashionFinds.service.ProductService;
import lombok.RequiredArgsConstructor;
import org.springframework.stereotype.Service;

import java.util.Collections;
import java.util.List;

@RequiredArgsConstructor
@Service
public class ProductServiceImpl implements ProductService {
    private final ArticleRepository articleRepository;
    private final ProductTypeRepository productTypeRepository;

    private final GenderRepository genderRepository;

    @Override
    public List<Article> getAllArticles() {
        return articleRepository.findAll();
    }

    @Override
    public Article getArticleById(Long id) {
        return articleRepository.findById(id).get();
    }

    @Override
    public List<Article> getArticlesByProductType(String productTypeName) {
        ProductType productType = productTypeRepository.getArticlesByName(productTypeName);
        if (productType == null) {
            return Collections.emptyList();
        }
        return productType.getArticles();
    }

    @Override
    public List<Article> filterArticlesByGender(String filter) {
        List<Article> result = genderRepository.findByName(filter).getArticles();
        return result;
    }

    @Override
    public void editArticle(Article article) {
        articleRepository.save(article);
    }
}
