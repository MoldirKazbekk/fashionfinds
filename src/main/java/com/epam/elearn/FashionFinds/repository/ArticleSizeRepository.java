package com.epam.elearn.FashionFinds.repository;

import com.epam.elearn.FashionFinds.entity.ArticleSize;
import com.epam.elearn.FashionFinds.entity.ArticleSizeCompositeKey;
import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.stereotype.Repository;

@Repository
public interface ArticleSizeRepository extends JpaRepository<ArticleSize, ArticleSizeCompositeKey> {

}
