package com.epam.elearn.FashionFinds.repository;

import com.epam.elearn.FashionFinds.entity.ProductType;
import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.stereotype.Repository;

@Repository
public interface ProductTypeRepository extends JpaRepository<ProductType, Integer> {
    ProductType getArticlesByName(String productName);

}
