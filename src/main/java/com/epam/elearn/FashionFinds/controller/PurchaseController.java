package com.epam.elearn.FashionFinds.controller;

import com.epam.elearn.FashionFinds.dto.ProductInCart;
import com.epam.elearn.FashionFinds.service.CustomerService;
import com.epam.elearn.FashionFinds.service.PurchaseService;
import lombok.RequiredArgsConstructor;
import lombok.extern.slf4j.Slf4j;
import org.springframework.stereotype.Controller;
import org.springframework.ui.Model;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.PostMapping;
import org.springframework.web.bind.annotation.RequestParam;

import java.util.List;

@RequiredArgsConstructor
@Controller
@Slf4j
public class PurchaseController {
    private final PurchaseService purchaseService;
    private final CustomerService customerService;
    private final ProductController productController;

    @PostMapping("/order/{id}")
    public String addProductToCart(@RequestParam("size") String size,
                                   @RequestParam("amount") Integer amount,
                                   @PathVariable("id") Long articleId,
                                   Model model) {
        boolean isAdded = purchaseService.addProductToCart(articleId, size, amount);
        if (!isAdded) {
            model.addAttribute("errorMessage", "SORRY!:( There is not enough amount of - " + articleId + " for size " + size);
        } else {
            model.addAttribute("infoMessage", "product with article - " + articleId + " was added to your cart successfully!");
            log.info("product with id - {}, amount - {}, size - {} was added to cart of user: {}", articleId, amount, size, customerService.getCurrentCustomer().getUsername());
        }
        return productController.viewProduct(articleId, model);
    }

    @GetMapping("/cart")
    public String showItemsInCart(Model model) {
        model.addAttribute("cartItems", purchaseService.getProductsInCart());
        model.addAttribute("total", purchaseService.getTotalSum());
        return "cart";
    }

    @PostMapping("/approveOrder")
    public String createPurchase(Model model) {
        purchaseService.createPurchase();
        model.addAttribute("infoMessage", "Your order is accepted!Thanks for choosing us");
        return "cart";
    }

    @PostMapping("/cart/removeItem")
    public String removeItemInCart(@RequestParam("articleId") String articleId, @RequestParam("sizeCode") String sizeCode, Model model) {
        List<ProductInCart> updatedList = purchaseService.removeItemInCart(Long.valueOf(articleId), sizeCode);
        model.addAttribute("cartItems", updatedList);
        model.addAttribute("total", purchaseService.getTotalSum());
        return "cart";

    }
}
