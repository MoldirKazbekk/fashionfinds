package com.epam.elearn.FashionFinds.entity;

import jakarta.persistence.Column;
import jakarta.persistence.Embeddable;
import lombok.AllArgsConstructor;
import lombok.Data;
import lombok.NoArgsConstructor;

import java.io.Serializable;

@Embeddable
@Data
@NoArgsConstructor
@AllArgsConstructor
public class ArticleSizeCompositeKey implements Serializable {
    @Column(name = "article_id")
    private Long articleId;

    @Column(name = "size_code")
    private String sizeCode;
}
