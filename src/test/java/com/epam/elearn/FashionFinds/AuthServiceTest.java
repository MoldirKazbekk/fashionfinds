package com.epam.elearn.FashionFinds;

import com.epam.elearn.FashionFinds.dto.CustomerForm;
import com.epam.elearn.FashionFinds.config.SecurityConfig;
import com.epam.elearn.FashionFinds.service.AuthService;
import jakarta.transaction.Transactional;
import org.junit.jupiter.api.Test;
import org.junit.jupiter.api.extension.ExtendWith;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.boot.test.autoconfigure.web.servlet.AutoConfigureMockMvc;
import org.springframework.boot.test.context.SpringBootTest;
import org.springframework.test.context.junit.jupiter.SpringExtension;
import org.springframework.test.web.servlet.MockMvc;
import org.springframework.test.web.servlet.request.MockMvcRequestBuilders;
import org.springframework.test.web.servlet.result.MockMvcResultMatchers;

import static org.junit.jupiter.api.Assertions.assertFalse;
import static org.junit.jupiter.api.Assertions.assertTrue;
@ExtendWith(SpringExtension.class)
@Transactional
@SpringBootTest(webEnvironment = SpringBootTest.WebEnvironment.MOCK,
        classes = {FashionFindsApplication.class, SecurityConfig.class})
@AutoConfigureMockMvc
public class AuthServiceTest {
    @Autowired
    private AuthService authService;
    @Autowired
    private MockMvc mockMvc;

    @Test
    public void registerCustomer(){
        CustomerForm customerForm = new CustomerForm();
        customerForm.setEmail("namesurname@gmail.com");
        customerForm.setFirstName("firstName");
        customerForm.setLastName("lastName");
        customerForm.setGender('F');
        customerForm.setUsername("usernameTest");
        customerForm.setPassword1("passw");
        customerForm.setPassword2("passw");

        assertFalse(authService.validatePassword(customerForm));
        assertFalse(authService.isUserAlreadyExist(customerForm));

        authService.registerCustomer(customerForm);

        assertTrue(authService.isUserAlreadyExist(customerForm));

    }
    @Test
    public void testLoginPage() throws Exception {
        mockMvc.perform(MockMvcRequestBuilders.get("/login"))
                .andExpect(MockMvcResultMatchers.status().isOk())
                .andExpect(MockMvcResultMatchers.view().name("login"));
    }
}
