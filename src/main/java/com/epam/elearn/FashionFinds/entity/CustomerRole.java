package com.epam.elearn.FashionFinds.entity;

import jakarta.persistence.*;
import lombok.AllArgsConstructor;
import lombok.Data;
import lombok.NoArgsConstructor;

import java.util.List;

@Table(name = "customer_role")
@Data
@Entity
@AllArgsConstructor
@NoArgsConstructor
public class CustomerRole {
    @Id
    @GeneratedValue(strategy = GenerationType.IDENTITY)
    @Column(name = "id")
    private Integer id;
    @Column(name = "role")
    private String role;
    @ManyToOne
    @JoinColumn(name = "customer_id")
    private Customer customer;


}
