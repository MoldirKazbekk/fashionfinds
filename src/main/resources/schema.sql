CREATE TABLE IF NOT EXISTS product_type (
id SERIAL PRIMARY KEY,
name text,
CONSTRAINT unique_product_name UNIQUE (name)
);

CREATE TABLE IF NOT EXISTS customer(
id SERIAL PRIMARY KEY,
first_name text,
last_name text,
gender varchar(1),
username varchar(15),
password text,
email text,
CONSTRAINT unique_username UNIQUE (username)
);

ALTER SEQUENCE customer_id_seq RESTART WITH 2;

CREATE TABLE IF NOT EXISTS customer_role(
id SERIAL PRIMARY KEY,
customer_id INTEGER,
role TEXT,
FOREIGN KEY (customer_id) REFERENCES customer (id)
);

CREATE TABLE IF NOT EXISTS season(
id SERIAL PRIMARY KEY,
name text,
CONSTRAINT unique_season UNIQUE (name)
);

CREATE TABLE IF NOT EXISTS gender(
id SERIAL PRIMARY KEY,
name varchar(100)
);

CREATE TABLE IF NOT EXISTS article(
article_code BIGINT PRIMARY KEY,
type_id INTEGER,
season_id INTEGER,
price real,
country varchar(100),
gender_id INTEGER,
description text,
image BYTEA,
FOREIGN KEY (type_id) REFERENCES product_type (id),
FOREIGN KEY (season_id) REFERENCES season (id),
FOREIGN KEY (gender_id) REFERENCES gender (id)
);

CREATE TABLE IF NOT EXISTS article_size(
article_id BIGINT,
size_code varchar(3),
amount INTEGER,
FOREIGN KEY (article_id) REFERENCES article (article_code),
PRIMARY KEY (article_id, size_code)
);

CREATE TABLE IF NOT EXISTS purchase(
purchase_id SERIAL PRIMARY KEY,
customer_id INTEGER,
total_sum INTEGER,
purchased_date DATE,
FOREIGN KEY (customer_id) REFERENCES customer (id)
);

CREATE TABLE IF NOT EXISTS purchase_detail(
purchase_detail_id SERIAL PRIMARY KEY,
purchase_id INTEGER,
article_code BIGINT,
size_code varchar(3),
amount INTEGER,
price REAL,
FOREIGN KEY (purchase_id) REFERENCES purchase (purchase_id),
FOREIGN KEY (article_code) REFERENCES article (article_code)
);
