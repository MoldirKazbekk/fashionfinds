package com.epam.elearn.FashionFinds.entity;

import jakarta.persistence.*;
import lombok.AllArgsConstructor;
import lombok.Data;
import lombok.NoArgsConstructor;

import java.util.List;

@Table(name = "article")
@Data
@Entity
@AllArgsConstructor
@NoArgsConstructor
public class Article {
    @Id
    @Column(name = "article_code")
    private Long id;
    @Column(name = "description")
    private String description;
    @Column(name = "country")
    private String country;
    @Column(name = "price")
    private Double price;
    @ManyToOne(targetEntity = Season.class)
    @JoinColumn(name = "season_id")
    private Season season;
    @ManyToOne(targetEntity = ProductType.class)
    @JoinColumn(name = "type_id")
    private ProductType productType;
    @ManyToOne(targetEntity = Gender.class)
    @JoinColumn(name = "gender_id")
    private Gender gender;
    @OneToMany(mappedBy = "article", targetEntity = ArticleSize.class)
    private List<ArticleSize> articleSizeList;
}
