package com.epam.elearn.FashionFinds.repository;

import com.epam.elearn.FashionFinds.entity.CustomerRole;
import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.stereotype.Repository;

@Repository
public interface CustomerRoleRepository extends JpaRepository<CustomerRole, Integer> {

}
