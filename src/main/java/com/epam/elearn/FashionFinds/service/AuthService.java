package com.epam.elearn.FashionFinds.service;

import com.epam.elearn.FashionFinds.dto.CustomerForm;
import org.springframework.ui.Model;

public interface AuthService {
    void registerCustomer(CustomerForm customerForm);
    boolean validatePassword(CustomerForm customerForm);
    boolean isUserAlreadyExist(CustomerForm customerForm);
    boolean checkUsernameAndPasswordRetype(CustomerForm customerForm, Model model);

}
