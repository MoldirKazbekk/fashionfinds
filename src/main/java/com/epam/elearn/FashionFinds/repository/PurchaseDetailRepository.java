package com.epam.elearn.FashionFinds.repository;

import com.epam.elearn.FashionFinds.entity.PurchaseDetail;
import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.stereotype.Repository;

import java.util.List;

@Repository
public interface PurchaseDetailRepository extends JpaRepository<PurchaseDetail, Integer> {
    List<PurchaseDetail> findByArticleCode(Long id);
}
