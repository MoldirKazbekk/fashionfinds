package com.epam.elearn.FashionFinds.service;

import com.epam.elearn.FashionFinds.dto.ProductInCart;

import java.util.List;

public interface PurchaseService {
    boolean addProductToCart(Long articleId, String size, Integer amount);
    List<ProductInCart> getProductsInCart();

    void createPurchase();
    Double getTotalSum();
    List<ProductInCart> removeItemInCart(Long articleId, String sizeCode);
}
