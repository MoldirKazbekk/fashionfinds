package com.epam.elearn.FashionFinds.controller;

import com.epam.elearn.FashionFinds.entity.Article;
import com.epam.elearn.FashionFinds.service.CustomerService;
import com.epam.elearn.FashionFinds.service.ProductService;
import lombok.RequiredArgsConstructor;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Controller;
import org.springframework.ui.Model;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.ModelAttribute;
import org.springframework.web.bind.annotation.PostMapping;
import org.springframework.web.bind.annotation.RequestParam;

@Controller
@RequiredArgsConstructor
public class AdminSiteController {
    private final ProductService productService;
    private final CustomerService customerService;

    @GetMapping("/adminSite")
    public String adminPage(Model model) {
        model.addAttribute("articles", productService.getAllArticles());
        model.addAttribute("customers", customerService.getAllCustomers());
        return "stuffSite";
    }

    @PostMapping("/adminSite")
    public String searchConsultants(@RequestParam("productName") String productName, Model model) {
        model.addAttribute("articles", productService.getArticlesByProductType(productName));
        return "stuffSite";
    }

    @PostMapping("/adminSite/editArticle")
    public String editArticle(@ModelAttribute("article") Article article, Model model) {
        productService.editArticle(article);
        model.addAttribute("warning", ("Article" + article.getId() + " has changed!"));
        model.addAttribute("articles", productService.getAllArticles());
        return "stuffSite";
    }
}
